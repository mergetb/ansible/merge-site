# Merge Site Ansible Configuration


## Variables

| name | required | default | description |
| ---- | ---------| ------- | ----------- |
| key | **yes** | | commander TLS key |
| cert | **yes** | | commander TLS cert |
| ca_key | **yes** | | certificate authority TLS key |
| ca | **yes** | | certificate authority TLS cert |
| wgdkey | **yes** | | wireguard dameon TLS key |
| wgdcert | **yes** | | wireguard dameon TLS cert |
| dbhost | no | localhost | etcd host |
| dbport | no | 2379 | etcd port |
| listen | no | 0.0.0.0 | commander listening address |
| port | no | 6000 | commander listening port |
| install | no | no | install software |
| uninstall | no | no | uninstall software |


## Examples

```yaml
include_role:
  name: merge-site
vars:
  install: yes
  key: cmdr-key.pem
  cert: cmdr.pem
  wgdkey: wgd.pem
  wgdcert: wgd-key.pem
  ca: cmdr-ca.pem
  ca_key: cmdr-ca-key.pem
```

